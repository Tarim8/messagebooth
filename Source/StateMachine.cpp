/*
  ==============================================================================

    StateMachine.cpp
    Created: 14 Jun 2019 12:08:38pm
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "StateMachine.h"

void StateMachine::setState (StateMachine::State newState)
{
    static StringArray string { "PlayingHostMessage",
                                "PlayingBeep",
                                "RecordingHostMessage",
                                "RecordingGuestMessage",
                                "PlayingLastGuestMessage",
                                "Idle"};
    JUCE_ASSERT_MESSAGE_THREAD

    DBG ("New State:" << string[(int)newState]);
    previousState = state;
    state = newState;
    listeners.call (&Listener::stateChanged, state);
}
