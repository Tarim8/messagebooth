/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent (StateMachine& sm) : stateMachine (sm)
{
    handsetButton.onClick = [this]()
    {
        if (stateMachine.getState() == StateMachine::State::Idle)
            stateMachine.setState (StateMachine::State::PlayingHostMessage);
        else
            stateMachine.setState (StateMachine::State::Idle);
    };
    
    recordHostMessageButton.onClick = [this]()
    {
        stateMachine.setState (StateMachine::State::RecordingHostMessage);
    };

    
    playLastMessageButton.onClick = [this]()
    {
        stateMachine.setState (StateMachine::State::PlayingLastGuestMessage);
    };

    addAndMakeVisible (handsetButton);
    addAndMakeVisible (recordHostMessageButton);
    addAndMakeVisible (playLastMessageButton);

    setSize (600, 400);
}

MainComponent::~MainComponent()
{

}

//==============================================================================

void MainComponent::resized()
{
    auto r = getLocalBounds();

    handsetButton.setBounds (r.removeFromTop (30).reduced (5));
    recordHostMessageButton.setBounds (r.removeFromTop (30).reduced (5));
    playLastMessageButton.setBounds (r.removeFromTop (30).reduced (5));
}
