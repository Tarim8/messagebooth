/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../StateMachine.h"
#include "../audio/Audio.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component
{
public:
    //==============================================================================
    MainComponent (StateMachine& sm);
    ~MainComponent();

    //==============================================================================
    void resized() override;

private:
    //==============================================================================
    // Your private member variables go here...
    TextButton handsetButton                {"Handset Up/Down"};
    TextButton recordHostMessageButton      {"Record Host Message"};
    TextButton playLastMessageButton        {"Play Last Message"};

    StateMachine& stateMachine;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
