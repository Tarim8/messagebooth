/*
  ==============================================================================

    StateMachine.h
    Created: 14 Jun 2019 12:08:38pm
    Author:  Tom Mitchell

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class StateMachine
{
public:
    enum class State
    {
        PlayingHostMessage,
        PlayingBeep,
        RecordingHostMessage,
        RecordingGuestMessage,
        PlayingLastGuestMessage,

        Idle
    };

    StateMachine() {}

    void setState (StateMachine::State newState);
    State getState() const {return state;}
    State getPreviousState() const {return previousState;}

    class Listener
    {
    public:
        Listener() = default;
        virtual ~Listener() = default;
        virtual void stateChanged (StateMachine::State newState) = 0;
    };

    void addListener (StateMachine::Listener* listener)
    {
        JUCE_ASSERT_MESSAGE_THREAD
        listeners.add (listener);
    }

    void removeListener (StateMachine::Listener* listener)
    {
        JUCE_ASSERT_MESSAGE_THREAD
        listeners.remove (listener);
    }

private:
    State state = {State::Idle};
    State previousState = {State::Idle};
    ListenerList<Listener> listeners;
};
