/*
  ==============================================================================

    Audio.h
    Created: 10 Apr 2019 3:28:04pm
    Author:  Tom Mitchell

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../StateMachine.h"
#include "FileRecorder.h"
#include "FilePlayer.h"
#include "../gpio/Gpio.h"

class Audio :   private StateMachine::Listener,
                private FilePlayer::Listener

{
public:
    Audio (StateMachine& sm);
    ~Audio();
    
private:    
    void playHostMessage();
    void playBeep();
    void recordMessage (const File& destinationFile);
    
    void stateChanged (StateMachine::State newState) override;
    void playbackStopped() override;
    
    File getGuestDirectory() const;
    
    StateMachine& stateMachine;
    
    AudioDeviceManager audioDeviceManager;
    FileRecorder fileRecorder;
    FilePlayer filePlayer;
    AudioSourcePlayer audioSourcePlayer;
    File lastGuestMessageFile;
};
