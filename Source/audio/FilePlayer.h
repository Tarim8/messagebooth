/*
  ==============================================================================
    FilePlayer.h
  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

/** Simple FilePlayer class - strams audio from a file. */
class FilePlayer :  public AudioSource,
                    public ChangeListener
{
public:
    /** Constructor */
    FilePlayer();
    
    /** Destructor */
    ~FilePlayer();
    
    /** Starts or stops playback of the looper */
    void setPlaying (bool newState);
    
    /** Gets the current playback state of the looper */
    bool isPlaying() const;
    
    /** Loads the specified file into the transport source */
    void loadFile (const File& newFile);
    
    class Listener
    {
        public:
        virtual ~Listener() {}
        virtual void playbackStopped() = 0;
    };
    
    void addListener (Listener* listenerToAdd)
    {
        listeners.add (listenerToAdd);
    }
    
    void removeListener (Listener* listenerToRemove)
    {
        listeners.remove (listenerToRemove);
    }
    
    //AudioSource
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
    
    //ChangeListener
    void changeListenerCallback (ChangeBroadcaster* source) override
    {
        if (! audioTransportSource.isPlaying())
            listeners.call (&Listener::playbackStopped);
    }
    
private:
    std::unique_ptr<AudioFormatReaderSource> currentAudioFileSource;    //reads audio from the file
    AudioTransportSource audioTransportSource;	        // this controls the playback of a positionable audio stream, handling the
                                                        // starting/stopping and sample-rate conversion
    TimeSliceThread thread;                             //thread for the transport source
    
    ListenerList<Listener> listeners;
};
