/*
  ==============================================================================

    Audio.cpp
    Created: 10 Apr 2019 3:28:04pm
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio(StateMachine& sm) :    stateMachine (sm)
{
    auto& deviceTypes = audioDeviceManager.getAvailableDeviceTypes();
    
    for (auto dt : deviceTypes)
    {
        DBG (dt->getTypeName());
        auto deviceNames = dt->getDeviceNames();
        for (auto dn : deviceNames)
        {
            DBG ("   " << dn);
        }
    }
    
    filePlayer.addListener (this);
    audioSourcePlayer.setSource (&filePlayer);
    audioDeviceManager.addAudioCallback (&audioSourcePlayer);
    audioDeviceManager.addAudioCallback (&fileRecorder);
    
    auto error = audioDeviceManager.initialise (2, 2, nullptr, false, "USB Audio Device, USB Audio; Front speakers");
    if ( ! error.isEmpty())
        DBG("Error starting audio device:" << error);
    

    
    stateMachine.addListener (this);
}

Audio::~Audio()
{
    stateMachine.removeListener (this);
    audioDeviceManager.closeAudioDevice();
    audioDeviceManager.removeAudioCallback (&fileRecorder);
    audioDeviceManager.removeAudioCallback (&audioSourcePlayer);
    audioSourcePlayer.setSource (nullptr);
}

//Private
void Audio::playHostMessage()
{
    File hostFile = File::getSpecialLocation (File::currentExecutableFile).getSiblingFile ("host.wav");
    
    if (! hostFile.exists())
        hostFile = hostFile.getSiblingFile ("host_default.wav");
    
    if (! hostFile.exists())
    {
        DBG ("Error loading host file: " << hostFile.getFullPathName());
        return;
    }

    filePlayer.loadFile (hostFile);
    filePlayer.setPlaying (true);
}

void Audio::playBeep()
{
    File beepFile = File::getSpecialLocation (File::currentExecutableFile).getSiblingFile ("beep.wav");
    
    if (! beepFile.exists())
    {
        DBG ("Error loading host file: " << beepFile.getFullPathName());
        return;
    }
    
    filePlayer.loadFile (beepFile);
    filePlayer.setPlaying (true);
}

void Audio::recordMessage (const File& destinationFile)
{
    DBG ("Recording To: " << destinationFile.getFullPathName());
    fileRecorder.startRecording (destinationFile);
}

//////////////////
void Audio::stateChanged (StateMachine::State newState)
{
    if (newState == StateMachine::State::PlayingHostMessage)
    {
        playHostMessage();
    }
    else if (newState == StateMachine::State::PlayingBeep)
    {
        playBeep();
    }
    else if (newState == StateMachine::State::PlayingLastGuestMessage)
    {
        if (stateMachine.getPreviousState() == StateMachine::State::Idle)
        {
            std::cout << "Pick up the headset" << std::endl;
            stateMachine.setState (StateMachine::State::Idle);
        }
        else 
        {
            if (fileRecorder.isRecording())
                fileRecorder.stop();
            
            if (! lastGuestMessageFile.exists())
            {
                DBG ("No guest messages avaialble");
            }
            else
            {
                filePlayer.loadFile (lastGuestMessageFile);
                filePlayer.setPlaying (true);
            }
        }
    }
    else if (newState == StateMachine::State::RecordingHostMessage)
    {
        if (stateMachine.getPreviousState() == StateMachine::State::Idle)
        {
            std::cout << "Pick up the headset" << std::endl;
            stateMachine.setState (StateMachine::State::Idle);
        }
        else 
        {
            if (filePlayer.isPlaying())
                filePlayer.setPlaying (false);
                
            File hostFile = File::getSpecialLocation (File::currentExecutableFile).getSiblingFile ("host.wav");
            recordMessage (hostFile);
        }
    }
    else if (newState == StateMachine::State::RecordingGuestMessage)
    {
        auto time = Time::getCurrentTime();
        auto fileName = time.formatted ("%Y-%m-%d-%H-%M-%S");
        
        lastGuestMessageFile = getGuestDirectory();
        
        if (lastGuestMessageFile.isDirectory())
            lastGuestMessageFile = lastGuestMessageFile.getChildFile ("GuestMessages/" + fileName + ".wav");
        else
            lastGuestMessageFile = File::getSpecialLocation (File::currentExecutableFile).getSiblingFile ("GuestMessages/" + fileName + ".wav");
            
        recordMessage (lastGuestMessageFile);
    }
    else if (newState == StateMachine::State::Idle)
    {
        const auto previousState = stateMachine.getPreviousState();
        if (previousState == StateMachine::State::PlayingHostMessage
            || previousState == StateMachine::State::PlayingBeep
            || previousState == StateMachine::State::PlayingLastGuestMessage)
            filePlayer.setPlaying (false);
        else if (previousState == StateMachine::State::RecordingHostMessage
                 || previousState == StateMachine::State::RecordingGuestMessage)
            fileRecorder.stop();
    }
}

void Audio::playbackStopped()
{
    const auto state = stateMachine.getState();
    
    if (state == StateMachine::State::PlayingHostMessage)
        stateMachine.setState (StateMachine::State::PlayingBeep);
    else if (state == StateMachine::State::PlayingBeep)
        stateMachine.setState (StateMachine::State::RecordingGuestMessage);
    else if (state == StateMachine::State::PlayingLastGuestMessage)
        stateMachine.setState (StateMachine::State::Idle);
}

File Audio::getGuestDirectory() const
{
        //create directory for guest messages
    
    File guestMessageDirectory = File ("/media/pi");
    
    Array<File> directories (guestMessageDirectory.findChildFiles (File::findDirectories, false));
    
    for (auto& d : directories)
    {
        DBG ("Parent:" << d.getFullPathName());
        Array<File> children (d.findChildFiles (File::findFilesAndDirectories , false, "*wedding*"));
        for (auto& c : children)
        {
            DBG ("Child:" << c.getFullPathName());
        }
        if (! children.isEmpty())
        {
            guestMessageDirectory = d;
            break;
        }
    }
    
    if (guestMessageDirectory.isDirectory())
    {
        DBG("Directory Found" << guestMessageDirectory.getFullPathName());
    }
    else
    {
        DBG("Cant Find Directory");
    }
    
    if (guestMessageDirectory.isDirectory())
    {
        DBG ("Found:" << guestMessageDirectory.getFullPathName());
        guestMessageDirectory.getChildFile ("GuestMessages").createDirectory();
    }
    else
    {
        DBG ("Device Not Found");
        File::getSpecialLocation (File::currentExecutableFile).getSiblingFile ("GuestMessages").createDirectory();
    }
    
    return guestMessageDirectory;
}
