/*
  ==============================================================================

    Gpio.h
    Created: 19 May 2019 10:00:37pm
    Author:  pi

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../StateMachine.h"

#if JUCE_LINUX
#include <wiringPi.h>
#endif //JUCE_LINUX

class Gpio :    private Thread,
                private StateMachine::Listener
{
public:
    Gpio (StateMachine& sm) :   Thread ("Gpio Thread"),
                                stateMachine (sm)
    {
#if JUCE_LINUX
        wiringPiSetup();			// Setup the library
        pinMode(0, OUTPUT);		// Configure GPIO0 as an output
        pinMode(1, OUTPUT);		// Configure GPIO1 as an output
        pinMode(2, OUTPUT);		// Configure GPIO2 as an output

        pinMode(3, INPUT);		// Configure GPIO3 as an input
        pinMode(4, INPUT);		// Configure GPIO4 as an input
#endif //JUCE_LINUX
        startThread (4);
        stateMachine.addListener (this);
        blue();
    }

    ~Gpio()
    {
        stateMachine.removeListener (this);
        stopThread (500);
        clear();
    }

    void clear()
    {
#if JUCE_LINUX
        digitalWrite(0, 0);
        digitalWrite(1, 0);
        digitalWrite(2, 0);
#endif //JUCE_LINUX
    }

    void red()
    {
#if JUCE_LINUX
        digitalWrite(0, 1);
        digitalWrite(1, 0);
        digitalWrite(2, 0);
#endif //JUCE_LINUX
#if JUCE_MAC
        DBG ("red");
#endif //JUCE_MAC
    }

    void green()
    {
#if JUCE_LINUX
        digitalWrite(0, 0);
        digitalWrite(1, 1);
        digitalWrite(2, 0);
#endif //JUCE_LINUX
#if JUCE_MAC
        DBG ("green");
#endif //JUCE_MAC
    }

    void blue()
    {
#if JUCE_LINUX
        digitalWrite(0, 0);
        digitalWrite(1, 0);
        digitalWrite(2, 1);
#endif //JUCE_LINUX
#if JUCE_MAC
        DBG ("blue");
#endif //JUCE_MAC
    }

private:
    //Thread
    void run() override
    {
        while (! threadShouldExit())
        {
#if JUCE_LINUX
            bool newRedButtonState = digitalRead (3);
            bool newWhiteButtonState = digitalRead (4);

            if (newRedButtonState != redButtonState)
            {
                redButtonState = newRedButtonState;
                if (redButtonState == false)
                {
                    stateMachine.setState (StateMachine::State::RecordingHostMessage);
                }
                DBG ("redButtonState: " << (int)redButtonState);
            }

            if (newWhiteButtonState != whiteButtonState)
            {
                whiteButtonState = newWhiteButtonState;
                if (whiteButtonState == false)
                {
                    stateMachine.setState (StateMachine::State::PlayingHostMessage);
                }
                else
                {
                    stateMachine.setState (StateMachine::State::Idle);
                }
                DBG ("whiteButtonState: " << (int)whiteButtonState);
            }
#endif //JUCE_LINUX
            wait (50);
        }
    }

    void stateChanged (StateMachine::State newState) override
    {
        switch (newState)
        {
            case StateMachine::State::PlayingHostMessage:
            case StateMachine::State::PlayingBeep:
            case StateMachine::State::PlayingLastGuestMessage:
                green();
                break;
                
            case StateMachine::State::RecordingHostMessage:
            case StateMachine::State::RecordingGuestMessage:
                red();
                break;
                
            case StateMachine::State::Idle: 
                blue(); 
                break;
        }
    };

    StateMachine& stateMachine;
    std::atomic<bool> redButtonState    {true};
    std::atomic<bool> whiteButtonState  {false};
};

